package collectionExo;

import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		List<String> animals = new ArrayList<String>();
		animals.add("Dog");
		animals.add("Cat");
		animals.add("Horse");
		animals.add("Licorne");
		
		ListIterator li = animals.listIterator();
		while(li.hasNext())
			System.out.println(li.next());
		
		//Rechercher un element dans une ArrayList
		
		System.out.println(animals.contains("Dog"));
		System.out.println("-------------------------------------------");
		//Joindre deux arraylist
		
		ArrayList<Object> heros = new ArrayList<Object>();
		heros.add("Thor");
		heros.add("Captain America");
		heros.add("Iron man");
		
		ArrayList<Object> armes = new ArrayList<Object>();
		armes.add("marteau");
		armes.add("Sabre laser");
		armes.add("bouclier");
		
		ArrayList<Object> listeComplete = new ArrayList<Object>();
		listeComplete.addAll(armes);
		listeComplete.addAll(heros);
		
		System.out.println("Liste compl�te:");
		for(Object objet: listeComplete) {
			System.out.println(objet);
		}
		
		//LinkedList Exercices
		
		LinkedList<Object> maListe = new LinkedList<Object>();
		maListe.add(5);
		maListe.add("Bonjour");
		maListe.add("hello");
		for(Object objet: maListe) {
			System.out.println(objet);
		}
		
		//LinkedList ins�rer des �l�ments en 1ere et derniere position + poition d�termin�e
		LinkedList<Object> myList = new LinkedList<Object>();
		myList.add(5);
		myList.add("Bonjour");
		myList.addLast("Friday");
		myList.addFirst("hello");
		myList.add(1,"licorne");
		
		for(Object objet: myList) {
			System.out.println(objet);
		}
		System.out.println("---------------------------------------");
		//Hachage
		//Obtenir le nbre d'�l�ments dans un hashset
		HashSet<Object> hs = new HashSet<Object>();
		hs.add("bonjour");
		hs.add(69);
		hs.add("c");
		System.out.println(hs.size());
		
		//Vide le hashset
		HashSet<Object> hs1 = new HashSet<Object>();
		hs1.add("bonjour");
		hs1.add(69);
		hs1.add("c");
		System.out.println(hs1.removeAll(hs1));
		System.out.println("---------------------------------------");
		//Convertir un hashset en arraylist

		HashSet<Object> hs2 = new HashSet<Object>();
		hs2.add("bonjour");
		hs2.add(69);
		hs2.add("c");
		Object [] obj = hs2.toArray();
		for(Object o: obj) {
			System.out.println(o);
		}
	}

}
